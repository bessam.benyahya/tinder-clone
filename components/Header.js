import { View, Text, TouchableOpacity } from "react-native";
import React from "react";
import tw from "tailwind-rn";
import { Foundation, Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";

export default function Header({ title, callEnabled = false }) {
  const navigation = useNavigation();

  return (
    <View style={tw("p-2 flex-row items-center justify-between")}>
      <View style={tw("flex flex-row items-center")}>
        <TouchableOpacity style={tw("p-2")} onPress={() => navigation.goBack()}>
          <Ionicons name="chevron-back-outline" size={34} color="#f0a830" />
        </TouchableOpacity>
        <Text style={tw("text-2xl font-bold pl-2")}>{title}</Text>
      </View>

      {callEnabled && (
        <TouchableOpacity style={tw("rounded-full mr-4 p-3 bg-yellow-200")}>
          <Foundation name="telephone" size={24} color="#f0a830" />
        </TouchableOpacity>
      )}
    </View>
  );
}
