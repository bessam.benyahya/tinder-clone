import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyDMv-MOUC_Q3fqlCvYLU4oPmojIABQnzNU",
  authDomain: "tinder-clone-6b92a.firebaseapp.com",
  projectId: "tinder-clone-6b92a",
  storageBucket: "tinder-clone-6b92a.appspot.com",
  messagingSenderId: "928272807538",
  appId: "1:928272807538:web:23d6252e4323e1d663b4eb",
};

const app = initializeApp(firebaseConfig);
const auth = getAuth();
const db = getFirestore();

export { app, auth, db };
