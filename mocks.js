export const DUMMY_DEVELOPERS = [
  {
    id: 1,
    firstName: "Bessam",
    lastName: "Ben Yahia",
    occupation: "React Native Developer",
    photoURL:
      "http://www.leboncommerce.fr/blog/wp-content/uploads/2014/12/bassam-ben-yahya-leboncommerce.fr_.jpg",
    experience: "4",
  },
  {
    id: 2,
    firstName: "Jonathan",
    lastName: "Dupont",
    occupation: "Node.js Developer",
    photoURL:
      "https://solvers.fr/wp-content/uploads/2021/06/De%CC%81veloppement-Offshore-De%CC%81veloppement-Nearshore-Sous-Traitance-Informatique-Cest-Quoi-600x400.jpg",
    experience: "2",
  },
  {
    id: 3,
    firstName: "Idriss",
    lastName: "Aberkane",
    occupation: "React.js Developer",
    photoURL:
      "https://www.scanderia.com/content/uploads/2017/09/idriss-j-aberkane.jpg",
    experience: "4",
  },
];
