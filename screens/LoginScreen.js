import { View, Text, ImageBackground, TouchableOpacity } from "react-native";
import React from "react";
import useAuth from "../hooks/useAuth";
import { useNavigation } from "@react-navigation/core";
import tw from "tailwind-rn";

export default function LoginScreen() {
  const { signInWithGoogle } = useAuth();

  return (
    <View style={tw("flex-1")}>
      <ImageBackground
        source={require("../assets/full-js-background.png")}
        resizeMode="cover"
        style={tw("flex-1")}
      >
        <TouchableOpacity
          style={[
            tw("absolute bottom-40 w-52 bg-white p-4 rounded-2xl"),
            { marginHorizontal: "25%" },
          ]}
          onPress={signInWithGoogle}
        >
          <Text style={tw("text-center font-semibold")}>
            Sign in & find a dev
          </Text>
        </TouchableOpacity>
      </ImageBackground>
    </View>
  );
}
