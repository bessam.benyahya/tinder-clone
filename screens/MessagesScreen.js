import {
  View,
  SafeAreaView,
  TextInput,
  Button,
  FlatList,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import React, { useEffect, useState } from "react";
import Header from "../components/Header";
import useAuth from "../hooks/useAuth";
import { getMatchedUserInfo } from "../lib/generateId";
import { useRoute } from "@react-navigation/core";
import tw from "tailwind-rn";
import SenderMessage from "../components/SenderMessage";
import ReceiverMessage from "../components/ReceiverMessage";
import {
  addDoc,
  collection,
  serverTimestamp,
  onSnapshot,
  query,
  orderBy,
} from "@firebase/firestore";
import { db } from "../firebase";

export default function MessagesScreen() {
  const { user } = useAuth();

  const [input, setInput] = useState("");
  const [messages, setMessages] = useState("");

  const { params } = useRoute();
  const { matchDetails } = params;

  useEffect(() => {
    const unsubscribe = onSnapshot(
      query(
        collection(db, "matches", matchDetails.id, "messages"),
        orderBy("timestamp", "desc")
      ),
      (snapshot) =>
        setMessages(
          snapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
          }))
        )
    );

    return unsubscribe;
  }, [matchDetails, db]);

  const matchedUserInfo = getMatchedUserInfo(matchDetails?.users, user.uid);

  const sendMessage = () => {
    addDoc(collection(db, "matches", matchDetails.id, "messages"), {
      userId: user.uid,
      displayName: user.displayName,
      photoURL: user.photoURL,
      message: input,
      timestamp: serverTimestamp(),
    });

    setInput("");
  };

  return (
    <SafeAreaView style={tw("flex-1")}>
      <Header title={matchedUserInfo.displayName} callEnabled />

      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={tw("flex-1")}
        keyboardVerticalOffset={10}
      >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <FlatList
            style={tw("pl-4")}
            data={messages}
            inverted={-1}
            keyExtractor={(item) => item.id}
            renderItem={({ item: message }) =>
              message.userId === user.uid ? (
                <SenderMessage key={message.id} message={message} />
              ) : (
                <ReceiverMessage key={message.id} message={message} />
              )
            }
          />
        </TouchableWithoutFeedback>

        <View
          style={tw(
            "flex-row justify-between items-center border-t border-gray-200 px-5 py-2"
          )}
        >
          <TextInput
            style={tw("h-10 text-lg")}
            placeholder="Send message..."
            onChangeText={setInput}
            onSubmitEditing={sendMessage}
            value={input}
          />
          <Button title="Send" onPress={sendMessage} color="#f0a830" />
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}
