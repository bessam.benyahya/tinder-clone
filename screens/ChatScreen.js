import { View, Text, SafeAreaView } from "react-native";
import React from "react";
import Header from "../components/Header";
import ChatList from "../components/ChatList";

export default function ChatScreen() {
  return (
    <SafeAreaView>
      <Header title="Chat" />
      <ChatList />
    </SafeAreaView>
  );
}
