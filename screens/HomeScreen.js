import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Image,
  StyleSheet,
} from "react-native";
import React, { useEffect, useLayoutEffect, useRef, useState } from "react";
import { useNavigation } from "@react-navigation/core";
import useAuth from "../hooks/useAuth";
import {
  onSnapshot,
  doc,
  collection,
  setDoc,
  getDoc,
  getDocs,
  query,
  where,
  serverTimestamp,
} from "@firebase/firestore";
import { auth, db } from "../firebase";

import tw from "tailwind-rn";
import { AntDesign, Entypo, Ionicons } from "@expo/vector-icons";

import Swiper from "react-native-deck-swiper";
import { generateId } from "../lib/generateId";

export default function HomeScreen() {
  const navigation = useNavigation();
  const { user, logout } = useAuth();
  const [profiles, setProfiles] = useState([]);

  const swiperRef = useRef(null);

  useLayoutEffect(
    () =>
      onSnapshot(doc(db, "users", user.uid), (doc) => {
        const results = doc.data();
        if (!results) {
          navigation.navigate("Modal");
        }
      }),
    []
  );

  useEffect(() => {
    let unsubscribe;

    const fetchCards = async () => {
      const passesIds = await getDocs(
        collection(db, "users", user.uid, "passes")
      ).then((snapshot) => snapshot.docs.map((doc) => doc.id));

      const matchesIds = await getDocs(
        collection(db, "users", user.uid, "matches")
      ).then((snapshot) => snapshot.docs.map((doc) => doc.id));

      unsubscribe = onSnapshot(
        query(
          collection(db, "users"),
          where("id", "not-in", [...passesIds, ...matchesIds, user.uid])
        ),
        (snapshot) => {
          setProfiles(
            snapshot.docs
              // .filter((doc) => doc.id !== user.uid)
              .map((doc) => ({
                id: doc.id,
                ...doc.data(),
              }))
          );
        }
      );
    };
    fetchCards();

    return unsubscribe;
  }, [db]);

  const swipeLeft = (cartIndex) => {
    if (!profiles[cartIndex]) return;

    const userSwiped = profiles[cartIndex];

    setDoc(doc(db, "users", user.uid, "passes", userSwiped.id), userSwiped);
  };

  const swipeRight = async (cartIndex) => {
    if (!profiles[cartIndex]) return;

    const userSwiped = profiles[cartIndex];

    const loggedInProfile = await (
      await getDoc(doc(db, "users", user.uid))
    ).data();

    // check if the user match with you
    getDoc(doc(db, "users", userSwiped.id, "matches", user.uid)).then(
      (snapshot) => {
        if (snapshot.exists()) {
          // set matches
          setDoc(
            doc(db, "users", user.uid, "matches", userSwiped.id),
            userSwiped
          );

          // create a match
          setDoc(doc(db, "matches", generateId(user.uid, userSwiped.id)), {
            users: {
              [user.uid]: loggedInProfile,
              [userSwiped.id]: userSwiped,
            },
            userMatched: [user.uid, userSwiped.id],
            timestamp: serverTimestamp(),
          });

          // display match screen with profiles props
          navigation.navigate("Match", {
            loggedInProfile,
            userSwiped,
          });
        } else {
          // set matches
          setDoc(
            doc(db, "users", user.uid, "matches", userSwiped.id),
            userSwiped
          );
        }
      }
    );
  };

  return (
    <SafeAreaView style={tw("flex-1")}>
      <View style={tw("flex-row items-center justify-between px-5")}>
        <TouchableOpacity onPress={() => logout()}>
          <Image
            style={tw("h-10 w-10 rounded-full")}
            source={{ uri: user.photoURL }}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("Modal")}>
          <Image
            style={tw("h-8")}
            resizeMode="contain"
            source={require("../assets/full-js.png")}
          />
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate("Chat")}>
          <Ionicons name="chatbubbles-sharp" size={30} color="#f0a830" />
        </TouchableOpacity>
      </View>

      <View style={tw("flex-1 -mt-4")}>
        <Swiper
          ref={swiperRef}
          containerStyle={{ backgroundColor: "transparent" }}
          cards={profiles}
          stackSize={5}
          cardIndex={0}
          animateCardOpacity
          verticalSwipe={false}
          onSwipedLeft={swipeLeft}
          onSwipedRight={swipeRight}
          overlayLabels={{
            left: {
              title: "No",
              style: {
                label: {
                  textAlign: "right",
                  fontSize: 24,
                  color: "#dc2742",
                },
              },
            },
            right: {
              title: "Match",
              style: {
                label: {
                  textAlign: "left",
                  fontSize: 24,
                  color: "#29aba4",
                },
              },
            },
          }}
          renderCard={(card) =>
            card ? (
              <View style={tw("relative bg-white h-3/4 rounded-xl")}>
                <Image
                  style={tw("absolute top-0 h-full w-full rounded-xl")}
                  source={{ uri: card.photoURL }}
                />

                <View
                  style={[
                    tw(
                      "flex-row absolute bottom-0 items-center justify-between bg-white w-full h-20 px-6 py-2 rounded-b-xl"
                    ),
                    styles.cardShadow,
                  ]}
                >
                  <View>
                    <Text style={tw("text-xl font-bold")}>
                      {card.displayName}
                    </Text>
                    <Text>{card.masterSkill}</Text>
                  </View>
                  <Text style={tw("text-xl font-bold")}>
                    +{card.experience} years
                  </Text>
                </View>
              </View>
            ) : (
              <View
                style={[
                  tw(
                    "relative bg-white h-3/4 rounded-xl justify-center items-center"
                  ),
                  styles.cardShadow,
                ]}
              >
                <Text style={tw("font-bold pb-5")}>No more profiles</Text>

                <Image
                  style={tw("h-20 w-full")}
                  height={100}
                  width={100}
                  source={{ uri: "https://links.papareact.com/6gb" }}
                />
              </View>
            )
          }
        />
      </View>
      <View style={tw("flex flex-row justify-evenly")}>
        <TouchableOpacity
          style={tw(
            "items-center justify-center rounded-full w-16 h-16 bg-red-200"
          )}
          onPress={() => swiperRef.current.swipeLeft()}
        >
          <Entypo name="cross" size={24} color="#dc2742" />
        </TouchableOpacity>
        <TouchableOpacity
          style={tw(
            "items-center justify-center rounded-full w-16 h-16 bg-green-200"
          )}
          onPress={() => swiperRef.current.swipeRight()}
        >
          <AntDesign name="heart" size={24} color="#29aba4" />
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  cardShadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
});
