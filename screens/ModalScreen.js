import { View, Text, Image, TextInput, TouchableOpacity } from "react-native";
import React, { useState } from "react";
import useAuth from "../hooks/useAuth";
import { useNavigation } from "@react-navigation/core";
import { setDoc, doc, serverTimestamp } from "@firebase/firestore";

import tw from "tailwind-rn";
import { db } from "../firebase";

export default function ModalScreen() {
  const { user } = useAuth();
  const navigation = useNavigation();

  const [image, setImage] = useState(null);
  const [masterSkill, setMasterSkill] = useState(null);
  const [experience, setExperience] = useState(null);

  const incompleteForm = !image || !masterSkill || !experience;

  const updateUserProfile = () => {
    setDoc(doc(db, "users", user.uid), {
      id: user.uid,
      displayName: user.displayName,
      photoURL: image,
      masterSkill,
      experience,
      timestamp: serverTimestamp(),
    })
      .then(() => navigation.navigate("Home"))
      .catch((err) => alert(err.message));
  };

  return (
    <View style={tw("flex-1 items-center pt-5")}>
      <Image
        style={tw("h-10 w-full")}
        resizeMode="contain"
        source={require("../assets/full-js.png")}
      />

      <Text style={tw("text-xl text-gray-700 p-7")}>
        Welcome {user.displayName}
      </Text>

      <Text style={tw("text-center p-4 font-bold text-yellow-500")}>
        Step 1: The Profil Pic
      </Text>
      <TextInput
        style={tw("text-center text-xl pb-2")}
        placeholder="Enter a Profile pic URL"
        value={image}
        onChangeText={setImage}
      />

      <Text style={tw("text-center p-4 font-bold text-yellow-500")}>
        Step 2: Your Master Skill
      </Text>
      <TextInput
        style={tw("text-center text-xl pb-2")}
        placeholder="React.js? Node.js?.."
        value={masterSkill}
        onChangeText={setMasterSkill}
      />

      <Text style={tw("text-center p-4 font-bold text-yellow-500")}>
        Step 3: Experiences (Years)
      </Text>
      <TextInput
        style={tw("text-center text-xl pb-2")}
        placeholder="How many years?"
        value={experience}
        onChangeText={setExperience}
        keyboardType="numeric"
        maxLength={2}
      />

      <TouchableOpacity
        disabled={incompleteForm}
        style={[
          tw("w-64 p-3 rounded-xl absolute bottom-10 bg-yellow-500"),
          incompleteForm && tw("bg-gray-400"),
        ]}
        onPress={updateUserProfile}
      >
        <Text style={tw("text-center text-white text-xl")}>Update Profile</Text>
      </TouchableOpacity>
    </View>
  );
}
