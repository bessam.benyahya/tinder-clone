export const generateId = (id1, id2) => (id1 > id2 ? id1 + id2 : id2 + id1);

export const getMatchedUserInfo = (users, userLoggedInId) => {
  const newUsers = { ...users };

  delete newUsers[userLoggedInId];

  const [id, user] = Object.entries(newUsers).flat();

  return { id, ...user };
};
